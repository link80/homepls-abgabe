# HomePls

HomePls ist eine App, die es auf Basis von OpenData ermöglicht von einem beliebigen Standort eine Möglichkeit zu finden, nach Hause zu gelangen. Zusätzliche Informationen, wie ein geschätzter Preis, sind ebenfalls verfügbar.

## Getting Started

Die folgende Anleitung hilft Dir das Projekt vernünftig aufzusetzen und versetzt Dich in die Lage an dem Projekt mitzuarbeiten.

### Vorbereitungen

Es müssen folgende Vorbereitungen getroffen werden:

```
Android Studio muss installiert und konfiguriert sein
```

```
Bei Benutzung eines Emulators muss ein GPS Datensatz an das emulierte Gerät gepusht werden
```

### Installation

Eine Schritt-Für-Schritt Anleitung welche Dir hilft die Entwicklungsumgebung aufzusetzen

Folgende Schritte müssen ausgeführt werden

```
Android Studio 2.2 installieren
```

```
Projekt importieren (File -> Open -> Open File or Project)
```

```
Emulator konfigurieren (oder wahlweise Android-Gerät verbinden/konfigurieren)
```

Nach Ausführung des Projekts muss noch ein GPS-Signal weitergegeben werden

```
Emulator -> More -> Location -> Send
```

Somit wurde ein GPS-Signal an den Emulator weitergegeben.
Jetzt kann die Anwendung benutzt werden.

## Ausführen der Tests

Um Tests an dem System durchzuführen werden im Emulator vom Benutzer Eingaben getätigt.

## Deployment

Bitte befolge folgende Anweisungen unter diesem Link [Run Apps on a Hardware Device](https://developer.android.com/studio/run/device.html)

## Erstellt mit

* [AndroidStudio](https://developer.android.com/studio/index.html) - Verwendete IDE

## Versionierung

Wir benutzen [Bitbucket](http://bitbucket.org/) zur Versionierung. Für eine Übersicht aller verfügbaren Versionen, klicke [hier](https://bitbucket.org/aass16team07/homepls).

## Autoren

* **Jessica Concepcion** - *Programmierung/Design* - [Bitbucket](https://bitbucket.org/JessicaConcepcion/)
* **Peter Szabo** - *Programmierung/Design* - [Bitbucket](https://bitbucket.org/Righthoven/)

## Lizenz

Dieses Projekt unterliegt derzeit keiner Lizenz

## Erwähnungen

* Danke an alle Autoren von genutzten Fremd-Libraries