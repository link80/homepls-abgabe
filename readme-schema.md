# Projekt Titel

Ein Paragraph mit der Projekt-Beschreibung kommt hier hin

## Getting Started

Hier kommt eine Beschreibung der folgenden Punkte rein

### Vorbereitungen

Zu installierende Software inklusive Installationshinweisen

```
Beispiele
```

### Installation

Eine Schritt-Für-Schritt Anleitung von Beispielen die benötigt werden um die IDE lauffähig zu machen

Angaben welche Schritte folgen

```
Beispiele
```

```
Beispiele
```

## Ausführen der Tests

Erklärung wie man (automatisierte) Tests für das System baut/durchführt

## Deployment

Hinzufügen von zusätzlichen Hinweisen wie man das System auf Hardware-Geräten lauffähig macht

## Erstellt mit

Beispiele von Fremdsoftware

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Versionierung

Angaben zur Versionierung des Systems (z.B. "Wir benutzen [Bitbucket](http://bitbucket.org/) zur Versionierung.")

## Autoren

* **Hannes Voss** - *Tätigkeitsbeschreibung* - [HannesVoss](https://bitbucket.org/link80)

## Lizenz

Angaben zur Lizensierung dieses Projekts

## Erwähnungen

* Hier kommen zusätzliche Personen rein
* die über Fremdsoftware
* zu dem Projekt beigetragen haben